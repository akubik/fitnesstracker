// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBqXq-fg40o6MFkFUJz_9UnLHedyGyqoSE",
    authDomain: "fitnesstracker-3cb0e.firebaseapp.com",
    databaseURL: "https://fitnesstracker-3cb0e.firebaseio.com",
    projectId: "fitnesstracker-3cb0e",
    storageBucket: "fitnesstracker-3cb0e.appspot.com",
    messagingSenderId: "106780465664"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
